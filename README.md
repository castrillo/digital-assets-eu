# Digital Assets [Forked]

Cryptocurrencies prices and statistics.

Crypto market cap & pricing data provided by Nomics (https://nomics.com)

![Screenshot of Digital Assets](screenshot.png)

## Author

Created in 2019 by François Grabenstaetter <francoisgrabenstaetter@gmail.com>

## Forked by:
Jose Castrillo for testing and improvement some CI/CD implementations, under GNU GPL 3.0 License.

## License
This program is distributed under the GNU GPL 3.0 License.
For more informations: https://www.gnu.org/licenses/gpl-3.0.en.html

## Installation and run:

### From Flathub
A Flatpak version is available here: https://flathub.org/apps/details/fr.fgrabenstaetter.DigitalAssets
	flatpak install flathub fr.fgrabenstaetter.DigitalAssets
	flatpak run fr.fgrabenstaetter.DigitalAssets

### From source - Debian OS testing

OS: [Linux debian10-uni 4.19.0-16-686-pae 1 SMP Debian 4.19.181-1 i686 GNU/Linux]

**Compiler list:**

```bash
git
make*
python3
meson 0.57 ---> from repo: deb http://http.us.debian.org/debian sid main non-free contrib
cmake
glib
cmake-devel
libgirepository1.0-dev
python3-cairo-dev
appstream-util
```

**Compiling:**

```bash
	mkdir build && cd build
	meson ..
	sudo ninja install
	digital-assets
```

purge and restore app for testing:

purge_all:
```
sudo ninja uninstall && cd /home/jose/app && rm -rf digital-assets-eu && sudo rm -rf /usr/local/lib/python3.7/site-packages/dassets
```

install_all
```
git clone https://gitlab.com/castrillo/digital-assets-eu.git && cd digital-assets-eu/build && chmod 700 compilar.sh && ./compilar.sh
```


## Troubleshooting

**error: If you get the following message when you issue ./digital-assets**

```bash
(digital-assets:25084): GLib-GIO-ERROR **: 16:38:36.255: Settings schema 'fr.fgrabenstaetter.DigitalAssets' is not installed
Trace/breakpoint trap
```

solution:
```bash
sudo /usr/bin/glib-compile-schemas /usr/local/share/glib-2.0/schemas/
```

**TypeError: 'dict_keys' object is not subscriptable**
```bash
Traceback (most recent call last):
  File "/usr/local/bin/digital-assets", line 33, in <module>
    Application()
  File "/usr/local/lib/python3.7/site-packages/dassets/ui/application.py", line 63, in __init__
    self.__mainWindow = Window(self)
  File "/usr/local/lib/python3.7/site-packages/dassets/ui/window.py", line 48, in __init__
    quoteCurrencySymbol = (self.currencies.keys())[0]
TypeError: 'dict_keys' object is not subscriptable
```

To fix it, I edited at *window.py* the line: 

```
quoteCurrencySymbol = self.currencies.keys()[0]
```
to:
```
quoteCurrencySymbol = list(self.currencies.keys())[0]
```