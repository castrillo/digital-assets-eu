��          �   %   �      P     Q     W     [     b  3   u  &   �     �     �     �  
   �  
   �          	       H        f     l     q  9   z  D   �  9   �  	   3     =     D     L  �  Q  	   =     G  	   L     V  1   k  '   �     �     �     �     �  	   �          	       V        v     {     �  G   �  V   �  C   +  	   o     y     �     �                                    
                                                                           	              About All Change Circulating Supply Crypto market cap & pricing data provided by Nomics Cryptocurrencies prices and statistics Day HTTP error occurred Hour Market Cap Max Supply Month Name Nomics API Key Please choose a new Nomics API key, as the default one can be overloaded Price Rank Settings There is a network problem, please verify your connection Too many requests with current Nomics API key, please take a new one Unauthorized access to Nomics API, please verify your key Undefined Volume Website Year Project-Id-Version: Digital Assets
PO-Revision-Date: 2021-08-29 15:57+0200
Last-Translator: François Grabenstaetter <francoisgrabenstaetter@gmail.com>
Language-Team: François Grabenstaetter <francoisgrabenstaetter@gmail.com>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
X-Generator: Poedit 3.0
X-Poedit-Basepath: ../../../../dassets
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SearchPath-0: .
 À propos Tout Variation Offre en circulation Market cap & données de prix fournies par Nomics Prix et statistiques des cryptomonnaies Jour Une erreur HTTP est survenue Heure Cap. Marché Offre Max Mois Nom Clé d'API Nomics Veuillez choisir une nouvelle clé d'API, car celle par défaut peut être surchargée Prix Rang Paramètres Un problème de réseau est survenu, veuillez vérifier votre connexion Trop de requêtes avec la clé d'API Nomics actuelle, veuillez en prendre une nouvelle Accès non autorisé à l'API Nomics, veuillez vérifier votre clé Indéfini Volume Site internet Année 